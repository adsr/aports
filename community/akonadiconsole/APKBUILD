# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=akonadiconsole
pkgver=22.04.1
pkgrel=0
# armhf blocked by extra-cmake-modules
# s390x, ppc64le and riscv64 blocked by akonadi
# ppc64le blocked by calendarsupport
arch="all !armhf !s390x !ppc64le !riscv64"
url="https://kontact.kde.org/"
pkgdesc="Application for debugging Akonadi Resources"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	akonadi-contacts-dev
	akonadi-dev
	akonadi-search-dev
	calendarsupport-dev
	extra-cmake-modules
	kcalendarcore-dev
	kcompletion-dev
	kcompletion-dev
	kconfig-dev
	kconfigwidgets-dev
	kcontacts-dev
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	ki18n-dev
	kio-dev
	kitemmodels-dev
	kitemviews-dev
	kmime-dev
	ktextwidgets-dev
	kwidgetsaddons-dev
	kxmlgui-dev
	libkdepim-dev
	messagelib-dev
	qt5-qtbase-dev
	samurai
	xapian-bindings
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/release-service/$pkgver/src/akonadiconsole-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
82a041536847beac209c399d4c3fede5abaf3e4e376d0f1f3416637e256ced94493ff5e742c7969bf8b7d1442902bdf16ed93dc66cd718969d13484221238632  akonadiconsole-22.04.1.tar.xz
"
