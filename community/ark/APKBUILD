# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=ark
pkgver=22.04.1
pkgrel=0
pkgdesc="Graphical file compression/decompression utility with support for multiple formats"
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by polkit -> kio
arch="all !armhf !s390x !riscv64"
url="https://kde.org/applications/utilities/org.kde.ark"
license="GPL-2.0-only"
depends="
	lrzip
	p7zip
	unzip
	zip
	zstd
	"
makedepends="
	extra-cmake-modules
	karchive-dev
	kconfig-dev
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	kitemmodels-dev
	kparts-dev
	kpty-dev
	kservice-dev
	kwidgetsaddons-dev
	libarchive-dev
	libzip-dev
	qt5-qtbase-dev
	samurai
	shared-mime-info
	xz-dev
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/release-service/$pkgver/src/ark-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

# secfixes:
#   20.08.0-r1:
#     - CVE-2020-24654
#   20.04.3-r1:
#     - CVE-2020-16116


build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	# plugins-cliunarchivertest is broken since Qt 5.11
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "plugins-clirartest"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
e265491f32d0bee9968c4a54a91015b85b3a4513a3a0f9cf16ff15aeb9a1c79aa3a52e4e3441cb328251dff827858bda52b44f186d378600bad402debeb7cdf8  ark-22.04.1.tar.xz
"
