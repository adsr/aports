# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=cervisia
pkgver=22.04.1
pkgrel=0
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by polkit -> kparts
arch="all !armhf !s390x !riscv64"
url="https://kde.org/applications/development/org.kde.cervisia"
pkgdesc="A user friendly version control system front-end"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	kdesu-dev
	kdoctools-dev
	kiconthemes-dev
	kinit-dev
	kitemviews-dev
	knotifications-dev
	kparts-dev
	kwidgetsaddons-dev
	qt5-qtbase-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/cervisia-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
6d8e2e5a09278dd3352af4d66485e56232895dd33bcef4b9dbddf795111e214bf1bfeb10e3cd151e0eba39fc23f8c328082a4408e2d4d89b69d7870cd63f857d  cervisia-22.04.1.tar.xz
"
