# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kbackup
pkgver=22.04.1
pkgrel=0
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by polkit -> kio
arch="all !armhf !s390x !riscv64"
url="https://kde.org/applications/utilities/org.kde.kbackup"
pkgdesc="An application which lets you back up your data in a simple, user friendly way"
license="GPL-2.0-only"
makedepends="
	extra-cmake-modules
	karchive-dev
	kdoctools-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	knotifications-dev
	kwidgetsaddons-dev
	kxmlgui-dev
	qt5-qtbase-dev
	samurai
	shared-mime-info
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kbackup-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
c5decf49bdb6f6a5e20ea0c40004ece0a8e23ad43f2687067b65cfd4547a65b2d21764cc2c810e270dd62ee4eaf86fc22ef10e5d30ed62e5cad17aa90eb95e4f  kbackup-22.04.1.tar.xz
"
