# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kcachegrind
pkgver=22.04.1
pkgrel=0
# armhf blocked by extra-cmake-modules
# s390x and risc64 blocked by polkit -> kxmlgui
arch="all !armhf !s390x !riscv64"
url="https://kde.org/applications/development/org.kde.kcachegrind"
pkgdesc="A profile data visualization tool, used to determine the most time consuming parts in the execution of a program"
license="GPL-2.0-only AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	karchive-dev
	kconfig-dev
	kcoreaddons-dev
	kdbusaddons-dev
	kdoctools-dev
	ki18n-dev
	kio-dev
	kwidgetsaddons-dev
	kxmlgui-dev
	qt5-qtbase-dev
	qt5-qttools-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kcachegrind-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
976bb2862727a0eca54dc21ed7f3b1c96b61d711020c042e686fa86a9a3fd6c01996374d479198f970cd8f4cfe7e28e3ca20715f5a212310653ac34a245913d9  kcachegrind-22.04.1.tar.xz
"
