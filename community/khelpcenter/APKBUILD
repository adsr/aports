# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=khelpcenter
pkgver=22.04.1
pkgrel=0
pkgdesc="Application to show KDE Applications' documentation"
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by polkit -> khtml
arch="all !armhf !s390x !riscv64"
url="https://userbase.kde.org/KHelpCenter"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	grantlee-dev
	karchive-dev
	kbookmarks-dev
	kconfig-dev
	kcoreaddons-dev
	kdbusaddons-dev
	kdoctools-dev
	khtml-dev
	ki18n-dev
	kinit-dev
	kservice-dev
	kwindowsystem-dev
	libxml2-dev
	qt5-qtbase-dev
	samurai
	xapian-core-dev
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/khelpcenter-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
f3500af124ecfd95f6533ac515241a16fef659b4963f54de037cef679abb44e8f0ee426f8320ea13646a7f953b4e24fd03b0f7c2036d1cfefefb41d9bdbb2471  khelpcenter-22.04.1.tar.xz
"
