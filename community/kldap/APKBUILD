# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kldap
pkgver=22.04.1
pkgrel=0
pkgdesc="LDAP access API for KDE"
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by polkit -> kio
arch="all !armhf !s390x !riscv64"
url="https://github.com/kde/kldap"
license="LGPL-2.0-or-later"
depends_dev="
	kcompletion-dev
	kdoctools-dev
	ki18n-dev
	kio-dev
	kwidgetsaddons-dev
	openldap-dev
	qt5-qtkeychain-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	cyrus-sasl-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kldap-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}

sha512sums="
2206d5bc9cf62311fb4b2368610323b4637573b105b1469026700d0dbb846af518f316d3fb437e94a28a9f7be8c685b5385870b428529bd55b90e91c00aed194  kldap-22.04.1.tar.xz
"
