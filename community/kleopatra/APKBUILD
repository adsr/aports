# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kleopatra
pkgver=22.04.1
pkgrel=0
# riscv64 disabled due to missing rust in recursive dependency
arch="all !armhf !s390x !riscv64"
url="https://www.kde.org/applications/utilities/kleopatra/"
pkgdesc="Certificate Manager and Unified Crypto GUI"
license="GPL-2.0-or-later AND GFDL-1.2-only"
depends="
	gnupg
	pinentry-qt
	"
makedepends="
	boost-dev
	extra-cmake-modules
	gpgme-dev
	kcmutils-dev
	kcodecs-dev
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	kdoctools-dev
	ki18n-dev
	kiconthemes-dev
	kitemmodels-dev
	kmime-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	kxmlgui-dev
	libassuan-dev
	libkleo-dev
	qgpgme
	qt5-qtbase-dev
	samurai
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/release-service/$pkgver/src/kleopatra-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build

	# kuniqueservicetest requires running dbus
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "kuniqueservicetest"
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}

sha512sums="
94bb859c472f076d5488fabf0dd6eb4b5d8eba5ef6ba795cbec2524339c92c832ef7edf99fc96f3b9be65968338c84cbb9ce3ef3f5f094d6b274a78ea294b0b6  kleopatra-22.04.1.tar.xz
"
