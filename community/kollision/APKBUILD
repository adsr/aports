# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kollision
pkgver=22.04.1
pkgrel=0
pkgdesc="A simple ball dodging game"
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by polkit -> kxmlgui
arch="all !armhf !s390x !riscv64"
url="https://kde.org/applications/games/kollision/"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	kconfig-dev
	kcoreaddons-dev
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	ki18n-dev
	kwidgetsaddons-dev
	kxmlgui-dev
	libkdegames-dev
	qt5-qtbase-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kollision-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
a36c42cb64ec640654a8967a932ed0f7e2d1490eb459dbed6e31da8c8c48e3216b62d8394ea7134fa87fcd2644691d951c8fc989dc2054a4f37c6425306574de  kollision-22.04.1.tar.xz
"
