# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kontrast
pkgver=22.04.1
pkgrel=0
pkgdesc="Tool to check contrast for colors that allows verifying that your colors are correctly accessible"
# armhf blocked by qt5-qtdeclarative
# s390x and riscv64 blocked by polkit -> kdeclarative
# riscv64 disabled due to missing rust in recursive dependency
arch="all !armhf !s390x !riscv64"
url="https://invent.kde.org/accessibility/kontrast"
license="GPL-3.0-or-later AND CC0-1.0"
depends="kirigami2"
makedepends="
	extra-cmake-modules
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qtquickcontrols2-dev
	qt5-qtsvg-dev
	kirigami2-dev
	ki18n-dev
	kcoreaddons-dev
	kdeclarative-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kontrast-$pkgver.tar.xz"
subpackages="$pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
ceaae7d2d183e9586ffa5088bfee80a292a0b61e5d53fe964c67bf421b461ad2875513f5e39a22b18b142d48ecff66fc3f7b145b9958cb6d2676c3a5537e65e7  kontrast-22.04.1.tar.xz
"
