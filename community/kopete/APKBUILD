# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kopete
pkgver=22.04.1
pkgrel=0
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by polkit -> kcmutils
arch="all !armhf !s390x !riscv64"
url="https://userbase.kde.org/Kopete"
pkgdesc="An instant messenger supporting AIM, ICQ, Jabber, Gadu-Gadu, Novell GroupWise Messenger, and more"
license="GPL-2.0-or-later AND LGPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	gpgme-dev
	kcmutils-dev
	kconfig-dev
	kcontacts-dev
	kcoreaddons-dev
	kcrash-dev
	kdbusaddons-dev
	kdelibs4support-dev
	kdnssd-dev
	kdoctools-dev
	kemoticons-dev
	khtml-dev
	ki18n-dev
	kidentitymanagement-dev
	kitemmodels-dev
	knotifyconfig-dev
	kparts-dev
	ktexteditor-dev
	kwallet-dev
	libkleo-dev
	qt5-qtbase-dev
	samurai
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/release-service/$pkgver/src/kopete-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
7617f841c1f5cb50e53f42c0534bb1b0d1a47799a224048172d7adee7b6ab1e47ee65b393a542187a2a6e67d6932fdaa57a3f0272ae60d7602b49175d80b9bb7  kopete-22.04.1.tar.xz
"
