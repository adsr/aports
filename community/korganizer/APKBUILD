# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=korganizer
pkgver=22.04.1
pkgrel=0
# armhf blocked by extra-cmake-modules
# ppc64le, s390x and riscv64 blocked by qt5-qtwebengine -> kmailtransport
arch="all !armhf !ppc64le !s390x !riscv64"
url="https://kontact.kde.org/components/korganizer.html"
pkgdesc="The calendar and scheduling component of Kontact"
license="GPL-2.0-or-later"
depends="kdepim-runtime"
makedepends="
	akonadi-calendar-dev
	akonadi-contacts-dev
	akonadi-dev
	akonadi-mime-dev
	akonadi-notes-dev
	akonadi-search-dev
	calendarsupport-dev
	eventviews-dev
	extra-cmake-modules
	incidenceeditor-dev
	kcalendarcore-dev
	kcalutils-dev
	kcmutils-dev
	kcodecs-dev
	kcompletion-dev
	kconfig-dev
	kconfigwidgets-dev
	kcontacts-dev
	kcoreaddons-dev
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	kholidays-dev
	kiconthemes-dev
	kidentitymanagement-dev
	kitemviews-dev
	kjobwidgets-dev
	kldap-dev
	kmailtransport-dev
	kmime-dev
	knewstuff-dev
	knotifications-dev
	kontactinterface-dev
	kparts-dev
	kpimtextedit-dev
	kservice-dev
	kuserfeedback-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	kxmlgui-dev
	libkdepim-dev
	phonon-dev
	pimcommon-dev
	qt5-qtbase-dev
	samurai
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/release-service/$pkgver/src/korganizer-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	# akonadi-sqlite-koeventpopupmenutest and akonadi-mysql-koeventpopupmenutest
	# require running dbus server
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "akonadi(-sqlite-koeventpopupmenu|-mysql-koeventpopupmenu)test"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
2fa66b984363472c387de22bc9d2feb3dc0c9591a4a1a33227a430757b2ef2258c0f998a2953fde27a7e25037e556d58d98316f48f0e9ff73214e2a840d7f382  korganizer-22.04.1.tar.xz
"
