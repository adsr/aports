# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kpmcore
pkgver=22.04.1
pkgrel=0
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by kauth
arch="all !armhf !s390x !riscv64"
url="https://kde.org/applications/system/org.kde.partitionmanager"
pkgdesc="Library for managing partitions"
license="GPL-3.0-or-later"
depends="
	device-mapper-udev
	sfdisk
	smartmontools
	"
makedepends="
	extra-cmake-modules
	kauth-dev
	kcoreaddons-dev
	ki18n-dev
	kwidgetsaddons-dev
	qca-dev
	qt5-qtbase-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kpmcore-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"
options="!check" # Requires running dbus server

# secfixes:
#   4.2.0-r0:
#     - CVE-2020-27187

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
ab9169b22eb7247669140715294ad81689e7ec4152ca32cf198f37a3e8cea99b44f04ff8193bfc6ec1fe51c07d735651979c1085cfb6b78e788a5738323c16f6  kpmcore-22.04.1.tar.xz
"
