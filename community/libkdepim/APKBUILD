# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=libkdepim
pkgver=22.04.1
pkgrel=0
pkgdesc="Lib for common KDEPim apps"
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by polkit -> kio
# ppc64le blocked by qt5-qtwebengine -> akonadi
arch="all !armhf !s390x !riscv64 !ppc64le"
url="https://community.kde.org/KDE_PIM"
license="GPL-2.0-or-later AND LGPL-2.0-or-later"
depends_dev="
	akonadi-contacts-dev
	akonadi-dev
	akonadi-search-dev
	kcmutils-dev
	kcodecs-dev
	kcompletion-dev
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	kitemviews-dev
	kjobwidgets-dev
	kldap-dev
	kwallet-dev
	kwidgetsaddons-dev
	qt5-qtbase-dev
	samurai
	"
makedepends="$depends_dev
	extra-cmake-modules
	qt5-qttools-dev
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/release-service/$pkgver/src/libkdepim-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_DESIGNERPLUGIN=ON
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
c15ddd41da32ac121fe488ea27cc368ade4abd977270b4bd7dc5c64ee9abf4c373d88c75ec468d1ed7530a43751c4a434671de951ec3c18e190bdcad8a45a552  libkdepim-22.04.1.tar.xz
"
