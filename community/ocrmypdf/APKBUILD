# Contributor: Carlo Landmeter <clandmeter@alpinelinux.org>
# Maintainer: Carlo Landmeter <clandmeter@alpinelinux.org>
pkgname=ocrmypdf
pkgver=13.4.5
pkgrel=0
pkgdesc="Add OCR text layer to scanned PDF files"
url="https://github.com/ocrmypdf/OCRmyPDF"
arch="noarch"
license="MIT"
options="!check" # missing pytest modules
depends="
	python3
	py3-cffi
	py3-chardet
	py3-coloredlogs
	py3-img2pdf
	py3-pdfminer
	py3-pikepdf
	py3-pillow
	py3-reportlab
	py3-tqdm

	ghostscript
	jbig2enc
	leptonica
	pngquant
	qpdf
	tesseract-ocr
	unpaper
	"
makedepends="py3-setuptools py3-setuptools_scm py3-setuptools-scm-git-archive"
checkdepends="py3-pytest py3-pytest-cov py3-pytest-xdist"
source="https://files.pythonhosted.org/packages/source/o/ocrmypdf/ocrmypdf-$pkgver.tar.gz"

prepare() {
	default_prepare
	sed -e '/setuptools_scm/d' \
		-e "/use_scm_version/cversion='$pkgver'," \
		-i setup.py
}

build() {
	python3 setup.py build
}

check() {
	python3 setup.py test
}

package() {
	python3 setup.py install --prefix=/usr --root="$pkgdir"
}

sha512sums="
27bd1806e11172e27149ffe16e9522dc4114375f9e8789ba78a814a784ba4d815e52bd43ea2d09b9e79022e40d464500920d1ed806cd1e820a7e70ee02d4def7  ocrmypdf-13.4.5.tar.gz
"
