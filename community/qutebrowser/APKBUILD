# Contributor: Leo <thinkabit.ukim@gmail.com>
# Maintainer: omni <omni+alpine@hack.org>
pkgname=qutebrowser
pkgver=2.5.1
pkgrel=0
pkgdesc="Keyboard-driven, vim-like browser based on PyQT5"
url="https://qutebrowser.org/"
# s390x, ppc64le and riscv64 blocked by qt5-qtwebengine
# armhf blocked by qt5-qtdeclarative -> py3-qt5
arch="noarch !armhf !s390x !ppc64le !riscv64"
license="GPL-3.0-only"
depends="
	py3-adblock
	py3-jinja2
	py3-pyqt5-sip
	py3-qt5
	py3-qtwebengine
	py3-yaml
	qt5-qtbase
	qt5-qtbase-sqlite
	qt5-qtwebengine
	"
makedepends="
	asciidoc
	py3-setuptools
	"
checkdepends="
	py3-hypothesis
	py3-pytest
	py3-pytest-benchmark
	py3-pytest-mock
	py3-pytest-rerunfailures
	"
subpackages="$pkgname-doc"
source="https://github.com/qutebrowser/qutebrowser/releases/download/v$pkgver/qutebrowser-$pkgver.tar.gz"
options="!check" # missing pytest dependencies

build() {
	a2x -f manpage doc/qutebrowser.1.asciidoc
	python3 setup.py build
}

check() {
	pytest
}

package() {
	make -f misc/Makefile DESTDIR="$pkgdir" PREFIX=/usr install
}

sha512sums="
27eca3e987e4c2a662fc863840d3ea697da65f55c7b9180e892466aeb5a2753f1eca424bbf8705670fe8cd9363e3297e03f9f9cc0d4f85f0198a4e3312d32f3c  qutebrowser-2.5.1.tar.gz
"
