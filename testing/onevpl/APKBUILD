# Contributor: psykose <alice@ayaya.dev>
# Maintainer: psykose <alice@ayaya.dev>
pkgname=onevpl
pkgver=2022.1.4
pkgrel=0
pkgdesc="oneAPI Video Processing Library"
url="https://github.com/oneapi-src/oneVPL"
arch="x86_64" # only x86_64 supported
license="MIT"
makedepends="cmake samurai"
subpackages="$pkgname-doc $pkgname-dev"
source="$pkgname-$pkgver.tar.gz::https://github.com/oneapi-src/oneVPL/archive/refs/tags/v$pkgver.tar.gz"
builddir="$srcdir/oneVPL-$pkgver"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DONEAPI_INSTALL_ENVDIR=/usr/share/oneVPL/env \
		-DONEAPI_INSTALL_LICENSEDIR=/usr/share/doc/oneVPL \
		-DONEAPI_INSTALL_MODFILEDIR=/usr/share/oneVPL/modulefiles \
		-DBUILD_PREVIEW=OFF \
		-DBUILD_EXAMPLES=OFF \
		-DBUILD_TOOLS=OFF \
		-DINSTALL_EXAMPLE_CODE=OFF \
		-DBUILD_TESTS="$(want_check && echo ON || echo OFF)"
	cmake --build build
}

check() {
	ctest -j $JOBS --output-on-failure --test-dir build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

dev() {
	default_dev

	amove usr/share
}

sha512sums="
14a258f05414ea3a9371beafdbcd909320b2fdd26caa76a39f914c5b1e8d67bfc6626757662ba9612b31a58fad68f01271d9fd5d007685b73e9f245341346d71  onevpl-2022.1.4.tar.gz
"
