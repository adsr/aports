# Contributor: psykose <alice@ayaya.dev>
# Maintainer: psykose <alice@ayaya.dev>
pkgname=rqlite
pkgver=7.5.0
pkgrel=0
pkgdesc="Lightweight, distributed relational database built on SQLite"
url="https://github.com/rqlite/rqlite"
arch="all !riscv64" # ftbfs
license="MIT"
makedepends="go"
checkdepends="python3 py3-requests"
options="!check" # a bunch of them fail due to runtime requirements
subpackages="
	$pkgname-doc
	$pkgname-bench
	$pkgname-client
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/rqlite/rqlite/archive/refs/tags/v$pkgver.tar.gz"

export GOFLAGS="$GOFLAGS -modcacherw -trimpath"

build() {
	go build ./cmd/rqbench
	go build ./cmd/rqlite
	go build ./cmd/rqlited
}

check() {
	RQLITED_PATH="$builddir"/rqlited python3 system_test/full_system_test.py
}

package() {
	install -Dm755 rqbench rqlite rqlited \
		-t "$pkgdir"/usr/bin

	install -D -m644 "$builddir"/DOC/*.md -t \
		"$pkgdir"/usr/share/doc/$pkgname
}

bench() {
	pkgdesc="$pkgdesc (benchmark utility)"

	amove usr/bin/rqbench
}


client() {
	pkgdesc="$pkgdesc (client)"

	amove usr/bin/rqlite
}

sha512sums="
970c9e1e861e4d5bcbfa0e1fad4b6937651bf60b87fec2d8f39eb2083fc87f706ff6da3de77e89e568e5288bfa189947924c0441d0cbc2eae014b54c7bb25394  rqlite-7.5.0.tar.gz
"
